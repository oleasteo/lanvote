# Requirements

## Node + npm

you need Node.js to run this application. NPM is bundled with each Node.js installation. Please
[download](http://nodejs.org/download/) and extract the bundle matching your system and put the bin folder on the $PATH.
For example run

    echo 'export PATH=$PATH:$HOME/[your/path/to/node.js/bin]' >> ~/.profile

## SQLITE3

We chose SQLITE3 as a lightweight DB solution for this project. It is available in many package management systems.
For Example on apt-based systems run

    sudo apt-get install sqlite3

## Imagemagick + Graphicsmagick

To process the images for the icons of your voting choices we use imagemagick/graphicsmagick. So please install
these two programs. For apt-base systems run

    sudo apt-get install imagemagick graphicsmagick

# Installation

    npm install -g grunt-cli coffee-script
    npm install
    grunt dist

# Start the server

    coffee server.coffee