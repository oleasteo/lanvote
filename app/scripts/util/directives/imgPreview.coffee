mod = angular.module 'util'

mod.directive 'imgPreview', ->
  restrict: 'E'
  scope:
    data: "="
    close: "&"
  template: '''
      <img-presenter ng-if="data.show" ng-click="close()" right-click="close()">
        <div ng-bind="data.text"></div>
        <img ng-src="{{data.url}}" src="" />
      </img-presenter>
    '''
  link: ($scope) ->
    $scope.close = ->
      $scope.data.show = false