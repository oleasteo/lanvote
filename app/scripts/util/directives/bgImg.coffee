mod = angular.module 'util'

mod.directive 'bgImg', ->
  link: (scope, element, attrs) ->
    element.css
      "background-image": "url(#{attrs.bgImg})"