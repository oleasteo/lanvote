app = angular.module 'open', ['ngRoute', 'common']

app.config ['$routeProvider', ($routeProvider) ->
  $routeProvider
  .when '/login',
      resolve:
        i18nLoad: ['i18n', (i18n) -> i18n.load()]
      controller: ['$scope', '$http', ($scope, $http) ->
        $http.get('/errors').then (res) ->$scope.error = res.data.error
      ]
      templateUrl: 'views/login.html'
  .when '/register',
      resolve:
        i18nLoad: ['i18n', (i18n) -> i18n.load()]
      controller: ['$scope', ($scope) ->
        $scope.$watch 'password', (password) ->
          $scope.passwordPattern = new RegExp(password || '(?=a)b')
      ]
      templateUrl: 'views/register.html'
  .otherwise
      redirectTo: '/login'
]