mod = angular.module 'closed'

mod.value 'colors', {
  colorizeElement: (elem, value, min, max) ->
    value -= min
    size = max - min
    factor = 255 / (size / 2)
    red = parseInt((size - value) * factor)
    green = parseInt(value * factor)
    elem.css 'background', 'rgb(' + red + ',' + green + ',0)'
}