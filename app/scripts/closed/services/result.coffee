mod = angular.module 'closed'

mod.factory 'result', [ 'voting', '$timeout', 'socket', '$location', 'games', (voting, $timeout, socket, $location, games) ->
  service =
    all: []
    winner: {}
    title: ''
    show: false
  currentVoteId = 0

  socket.on 'result', (data) ->
    $timeout ->
      service.display data
      voting.ready = false

  service.display = (data) ->
    voteId = ++currentVoteId
    service.all = data
    for id of data
      if data[id].winner
        service.winner = games.getById id
        break
    if service.winner?
      service.title = games.pathString service.winner.id, ' / '
      $location.path '/results'
      $timeout ->
        $location.path '/voting' if voteId == currentVoteId
      , 60000
    else
      $location.path '/voting'

  service
]