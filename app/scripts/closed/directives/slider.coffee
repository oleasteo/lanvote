mod = angular.module 'closed'

mod.directive 'slider', ['colors', (colors) ->
  restrict: 'EA'
  scope:
    value: "="
    min: "@"
    max: "@"
    step: "@"
  template: '<div class="slider-bounds"></div>'
  link: ($scope, $elem, $attrs) ->
    $scope.min = min = if $scope.min? then +$scope.min else 0
    $scope.max = max = if $scope.max? then +$scope.max else 100
    $scope.step = step = if $scope.step? then +$scope.step else 1
    showLabel = $attrs.label?
    colorize = $attrs.colorize?

    $slider = $elem.children '.slider-bounds'
    $slider.addClass 'show-label' if showLabel

    colors.colorizeElement $elem, $scope.value, min, max if colorize

    $slider.slider {
      slide: (e, ui) ->
        $scope.$apply ->
          $scope.value = ui.value
          colors.colorizeElement $elem, +ui.value, min, max if colorize
          updateLabel ui.value if showLabel
    }

    $handle = $slider.children '.ui-slider-handle'

    updateLabel = (value) ->
      $handle.text value
    updateLabel $scope.value

    updateRange = (oldRange, newRange) ->
      percentage = (value - oldRange.min) / (oldRange.max - oldRange.max)
      value = newRange.min + percentage * (newRange.max - newRange.min)
      updateLabel Math.round(value)

    watch = (attr, cb) ->
      $scope.$watch attr, (val) ->
        $slider.slider 'option', attr, +val
        cb? +val
    watch 'value'
    watch 'min', (val) ->
      min = val if val?
    watch 'max', (val) ->
      max = val if val?
    watch 'step', (val) ->
      step = val if val?
]