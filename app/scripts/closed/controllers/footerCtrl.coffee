mod = angular.module 'closed'

mod.controller 'footerCtrl', ['$scope', 'user', 'voting', 'chat',
  ($scope, user, voting, chat) ->
    $scope.chat = chat
    user.load().then ->
      $scope.status = user.status
    $scope.voting = voting
    $scope.$watch 'chat.show', (show) ->
      $scope.voting.footerClasses['show-chat'] = show
    $scope.$watch 'voting.ready', (ready) ->
      voting.footerClasses.ready = ready
      voting.setReady ready
]