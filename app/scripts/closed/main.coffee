app = angular.module 'closed', ['ngRoute', 'common']

app.config ['$routeProvider', ($routeProvider) ->
  $routeProvider
  .when '/voting',
      resolve:
        i18nLoad: ['i18n', (i18n) -> i18n.load()]
        statusLoad: ['user', (user) -> user.load()]
        gamesLoad: ['games', (games) -> games.load()]
        votingLoad: ['voting', 'result', (voting) -> voting.load()]
      templateUrl: 'views/voting.html'
      controller: 'votingCtrl'
  .when '/manage-games',
      resolve:
        i18nLoad: ['i18n', (i18n) -> i18n.load()]
        votingLoad: ['voting', 'result', (voting) -> voting.load()]
      templateUrl: 'views/manage-games.html'
      controller: 'manageGamesCtrl'
  .when '/results',
      resolve:
        i18nLoad: ['i18n', (i18n) -> i18n.load()]
      templateUrl: 'views/results.html'
      controller: 'resultsCtrl'
  .otherwise
      redirectTo: '/voting'
]