mod = angular.module 'closed'

mod.directive 'editGameWrapper', ['$compile', 'games', ($compile, games) ->
  restrict: 'E'
  scope:
    game: "="
    preview: '='
    button: '&'
  template: '
        <game>
          <img-wrapper ng-click="preview.showImg(game.image)" right-click="preview.showImg(game.icon)"><div bg-img="{{game.icon}}"></div></img-wrapper>
          <div ng-bind="game.title"></div>
          <label for="game.{{game.id}}.active" ng-bind="\'form.game.active\' | i18n"></label><input id="game.{{game.id}}.active" type="checkbox" ng-model="game.active"/>
          <span class="button" ng-if="game.variants.length" ng-bind="\'form.voting.variants.\' + button.text | i18n" ng-click="button.action()"></span>
          <button type="button" ng-bind="\'form.delete\' | i18n" ng-click="delete()"></button>
          <button type="button" ng-bind="\'form.edit\' | i18n" ng-click="edit = !edit"></button>
        </game>
        <edit-game game="game" ng-if="edit" edit="true" admin="true" save="update()"></edit-game>'
  controller: ['$scope', 'manageGames', ($scope, manageGames) ->
    $scope.edit = false
    $scope.update = ->
      $scope.edit = false
      manageGames.update $scope.game
    $scope.delete = ->
      manageGames.delete $scope.game
  ]
  link: ($scope, $elem) ->
    $scope.button =
      text: 'show'
      classes:
        expanded: false
      action: ->
        $scope.button.classes.expanded = !$scope.button.classes.expanded
        $scope.button.text = if $scope.button.classes.expanded then 'hide' else 'show'
    variants = $scope.game.variants || []
    if variants.length > 0
      $template = $ '<variants ng-class="button.classes"></variants>'
      variants.forEach (variant, idx) ->
        $template.append """<edit-game-wrapper game="game.variants[#{idx}]" preview="preview"></game-wrapper>"""
      $elem.append $compile($template) $scope
    $scope.$watch 'game.active', (newVal, oldVal) ->
      $scope.update() if newVal != oldVal
]