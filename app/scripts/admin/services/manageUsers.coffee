mod = angular.module 'admin'

mod.factory 'manageUsers', ['socket', 'toaster', '$timeout', (socket, toaster, $timeout) ->
  service = {}
  originalUsers = []
  socket.on 'not active', (data) ->
    toaster.info 'user.activate', [data]
  setUsers = (users) ->
    service.all = angular.copy users
    originalUsers = users
  service.load = ->
    socket.query 'users', setUsers
  service.update = (user) ->
    service.all.forEach (user) ->
      ou = originalUsers.filter((ou) -> ou.username == user.username)[0]
      if ou.active != user.active
        socket.emit 'user.activate', user
        ou.active = user.active
        toaster.remove 'info.user.activate', [user.username] if user.active
  socket.on 'users', setUsers
  service
]