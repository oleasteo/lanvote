mod = angular.module 'common'

mod.controller 'mainCtrl', ['$rootScope', '$scope', 'i18n', 'toaster', '$location',
  ($rootScope, $scope, i18n, toaster, $location) ->
    startLoading = ->
      $scope.pageLoading = true
    stopLoading = ->
      $scope.pageLoading = false
    $scope.toaster = toaster
    $scope.$on '$routeChangeStart', startLoading
    $scope.$on '$routeChangeSuccess', stopLoading
    $scope.$on '$routeChangeError', stopLoading
    $rootScope.$on '$locationChangeSuccess', -> $scope.currentPath = $location.path()
    i18n.load()
    $scope.i18n = i18n
    $scope.i18nData =
      locales: i18n.locales
      currentLocale: i18n.currentLocale
    $scope.$watch 'i18nData.currentLocale', (locale) ->
      i18n.load locale
]