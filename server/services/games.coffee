db = require('../sqlite').db
_ = require 'underscore'

_.extend exports,
  all: (cb) ->
    result = []
    error = null
    requestsPending = 0
    allRequestsSent = false
    db.all 'SELECT rowid AS id, * FROM games WHERE variantOf IS NULL ORDER BY title', (err, games) ->
      if games.length == 0
        cb null, []
      games.forEach (game, idx) ->
        error = err
        if !error?
          result.push game
          game.active = game.active == 1
          requestsPending++
          allRequestsSent = idx == games.length - 1
          db.all 'SELECT rowid AS id, * FROM games WHERE variantOf = ? ORDER BY title', game.id, (err, variants) ->
            requestsPending--
            error = err
            if !error?
              game.variants = variants
              variants.forEach (variant) ->
                variant.active = variant.active == 1
                variant.image = game.image if !variant.image?
                variant.icon = game.icon if !variant.icon?
              if allRequestsSent && !requestsPending
                cb error, result