_ = require 'underscore'

conns = {}

asArray = -> Object.keys(conns).map (username) -> conns[username]

bySocket = (socket) ->
  _.find conns, (c) -> c.sockets[socket.id]?

remove = (socket) ->
  conn = bySocket socket
  if conn?
    delete conn.sockets[socket.id]
    delete conns[conn.user.username] if _.isEmpty conn.sockets
  conn

add = (user, sock) ->
  conn = conns[user.username] = conns[user.username] ||
    sockets: {}
    user: user
    emit: ->
      args = arguments
      _.each this.sockets, (s) ->
        s.emit.apply s, args
  conn.sockets[sock.id] = sock
  conn

broadcastToAdmins = ->
  _.each conns.all, (conn) ->
    conn.emit.apply conn, arguments if conn.user.admin

_.extend exports,
  all: conns
  asArray: asArray
  bySocket: bySocket
  add: add
  remove: remove
  broadcastToAdmins: broadcastToAdmins