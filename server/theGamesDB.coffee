http = require 'http'
url = require 'url'
parseString = require('xml2js').parseString;
log = require('./utils/logger') 'theGamesDB'
_ = require 'underscore'

imagePattern = /\{[^}]*(\.jpg|\.png)[^}]*\}/g

getXML = (path, success, error) ->
  http.get(path, (httpRes) ->
    xml = ''
    httpRes.on 'data', (chunk) -> xml += chunk
    httpRes.on 'end', ->
      parseString xml, (err, json) ->
        if error? and err?
          error err
        else
          success json
  ).on 'error', (err) ->
    error err
    log.warn "could not reach host. #{err}"

getImages = (game, baseImageUrl) ->
  images = []
  find = (obj) ->
    if obj._
      images.push {
        url: baseImageUrl + obj._
        width: obj.$.width
        height: obj.$.height
      }
    else if typeof obj == 'object'
      for child of obj
        find obj[child]
  find game.Images
  images

parseGame = (game, imgPrefix) ->
  title: game.GameTitle[0]
  genre: game.Genres[0].genre[0] if game.Genres and game.Genres[0] and game.Genres[0].genre
  publisher: game.Publisher[0] if game.Publisher
  released: game.ReleaseDate[0] if game.ReleaseDate
  theGamesDbId: game.id[0]
  description: game.Overview
  images: getImages game, imgPrefix

processGamesJsonMaker = (cb) ->
  (json) ->
    list = json.Data.Game
    imgPrefix = json.Data.baseImgUrl
    result = []
    if list
      list.forEach (item) ->
        result.push parseGame item, imgPrefix
    cb result

request = (query, success) ->
  theGamesDbAPI =
    protocol: "http"
    host: "thegamesdb.net"
    pathname: "/api/GetGame.php"
    query: query
  getXML url.format(theGamesDbAPI), processGamesJsonMaker(success), -> success []

_.extend exports,
  query: request