express = require 'express'
app = express()
passport = require 'passport'
users = require './controllers/usersCtrl'
sec = require './utils/security'
log = require('./utils/logger') 'serverSetup'
_ = require 'underscore'
eb = require './utils/eventBus'
cookieParser = require 'cookie-parser'
bodyParser = require 'body-parser'
session = require 'express-session'
path = require 'path'

secret = '843omtu4j238mtj58u3428tpu432m8c'

app.use cookieParser() # for session cookie
app.use bodyParser.json()
app.use bodyParser.urlencoded extended: false
app.use session secret: secret, resave: false, saveUninitialized: false
app.use passport.initialize()
app.use passport.session()

configureApp = (secure) ->

  rootPath = process.cwd() + '/' + if secure then 'dist' else '.tmp'

  sendIndex = (req, res) ->
    if req.isAuthenticated()
      if req.user.admin
        res.sendFile "#{rootPath}/admin.html"
      else
        res.sendFile "#{rootPath}/closed.html"
    else
      res.sendFile "#{rootPath}/open.html"

  loginAsAdmin = (req, cb) ->
    if !secure and !req.isAuthenticated()
      log.debug "logging in as admin"
      users.getByUsername 'admin', (err, admin) ->
        req.login admin, cb
    else
      cb()

  sendFile = (req, res) ->
    res.sendFile "#{rootPath}#{req.path}"

  sendFileChecked = (req, res, check) ->
    if !secure
      loginAsAdmin req, ->
        sendFile req, res
    else if check()
      sendFile req, res
    else
      res.status(404).send 'not authorized'

  sendFileIfAuthenticated = (req, res) ->
    sendFileChecked req, res, -> req.isAuthenticated()
  sendFileIfAdmin = (req, res) ->
    sendFileChecked req, res, -> req.isAuthenticated() and req.user.admin

  getRoot = (req, res) ->
    if secure
      sendIndex req, res
    else
      loginAsAdmin req, -> res.sendFile "#{rootPath}/index.html"

  getLastError = (req, res) ->
    error = req.session.loginError
    res.send if error? then error: error else {}
    delete req.session.loginError

  requireSocketToken = (req, res) ->
    connectionManager = require './controllers/connectionCtrl'
    generateToken = ->
      token = sec.encrypt "#{sec.generateSalt()} --- #{secret} --- #{req.user.username}"
      connectionManager.addToken token, req.user
      res.send token: token
    if !secure
      loginAsAdmin req, generateToken
    else if req.isAuthenticated()
      generateToken()
    else
      res.status(401).send 'Please log in to connect to the socket.'

  handleMapFiles = (req, res) ->
    if secure
      res.status(404).send 'Not delivered.'
    else
      res.sendFile "#{rootPath}#{req.path}"

  logout = (req, res) ->
    req.logout()
    res.redirect '/'

  handleLogin = (req, res, next) ->
    passport.authenticate('local', (err, user, info) ->
      if err
        next(err)
      else if !user
        req.session.loginError = info.message
        res.redirect '/'
      else
        req.logIn user, (err) ->
          if err
            next(err)
          else
            res.redirect '/'
    )(req, res, next)

  handleRegister = (req, res) ->
    users.register req.body, (err) ->
      if !err
        users.getByUsername req.body.username, (err, user) ->
          eb.emit 'user.registered', user
          req.logIn user, -> res.redirect '/'
      else
        req.session.loginError = err

  routes =
    get:
      '/': getRoot
      '/errors': getLastError
      '/socketToken': requireSocketToken
      '/*.map': handleMapFiles
      '/logout': logout
      '/admin.html': sendFileIfAdmin
      '/scripts/admin.min.js': sendFileIfAdmin
      '/closed.html': sendFileIfAuthenticated
      '/scripts/closed.min.js': sendFileIfAuthenticated
      '*': sendFile
    post:
      '/login': handleLogin
      '/register': handleRegister


  _.each routes.get, (handler, path) ->
    app.get path, handler
  _.each routes.post, (handler, path) ->
    app.post path, handler

serverInitializers =
  http : ->
    configureApp false
    require('http').createServer app
  https: ->
    configureApp true
    fs = require 'fs'
    opts =
      key: fs.readFileSync 'ssl/key.pem'
      cert: fs.readFileSync 'ssl/cert.pem'
    require('https').createServer opts, app

createServer = (protocol) ->
  this.server = serverInitializers[protocol]()
  eb.emit 'server.created', this.server
  this.server

_.extend exports,
  createServer: createServer